#!/usr/bin/ruby

# Bubble-index -- a really simple audio indexer
# Copyright (c) 2006 David Baelde
# This software is under GNU GPL version 2

require File.dirname(__FILE__)+'/mytaglib.rb'
require 'rubygems'
require 'sqlite3'
include SQLite3

USAGE = <<USAGE
Usage: #{$0} [options] [paths]

Paths: A list of paths to explore, with optional "-p prefix" and "-r prefix"
    indicating how to modify entries' path. This is useful if you index files
    on a virtual filesystem (fusesmb,gnomevfs-mount,..), but want
    the full URL to appear in the database for being usable more generally.

  -p prefix
    Add the prefix to the local path.

  -r prefix
    Remove the prefix from the local path -- before prepending any -p prefix.

Options:

  -d file
    Path to a SQLite3 database file where to store data. If the file doesn't
    exist it will be created and prepared.
    Defaults to "bubble.sql".

  -m n
    Maximum number of failures before deletion of an entry in the database.
    An entry gets one more failure everytime an update happens without
    touching it.
    Defaults to 10 failures.

  -w n
    Do not update a file less than n days after previous update.
    Defaults to 3 days.

  -h
    Display this help.
USAGE

$db = "bubble.sql"
$max_failures = 10
$wait = 3
$paths = []
$prefix = ""
$rprefix = ""
while not ARGV.empty? do
  arg = ARGV.shift
  case arg
  when "-h"
    print USAGE
    exit 0
  when "-m"
    $max_failures = ARGV.shift
  when "-w"
    $wait = ARGV.shift
  when "-d"
    $db = ARGV.shift
  when "-r"
    $rprefix = ARGV.shift
  when "-p"
    $prefix = ARGV.shift
  else
    arg = arg.sub(/\/*$/,"")
    if $rprefix != "" and arg[0..$rprefix.length-1] != $rprefix then
      puts "Path #{arg} should start with #{$rprefix}!"
      exit 1
    end
    $paths.push [$prefix,$rprefix,arg]
  end
end

# == Database management ==================================================== #

# Note on database format: it would be possible to use only the last_seen field 
# to decide when to delete an entry. But then, suppose you haven't been 
# updating your database for a long time, and some machine is temporarily 
# unavailable at the time you update, it disappears. With the failure counter 
# you avoid that risk and deletion relies less on the frequency of database 
# updates.
# Another remark is that the directories table might seem useless. It is 
# actually useful to avoid scanning frequently a huge directory of non-audio 
# files.

create = !(File.exists? $db)
DB = Database.new($db)
if create then
  puts "Creating database #{$db}..."
  DB.execute_batch <<-SQL
    create table files (
      path      TEXT primary key,
      genre     TEXT,
      artist    TEXT,
      album     TEXT,
      title     TEXT,
      last_seen INTEGER,
      failures  INT
    ) ;
    create table directories (
      path      TEXT primary key,
      last_seen INTEGER,
      failures  INTEGER
    )
  SQL
end

# Retry to access a resource when it's busy
DB.busy_handler do |resource,retries|
  sleep 0.5
  if retries < 30 then 1 else 0 end
end

# Limit date after which entries should be updated
# At the end of the update, entries older than the LIMIT will get one failure
LIMIT = Time.now.tv_sec - 24*3600*$wait

# Check that some file or dir has not been updated too recently

def updatable(table,path)
  "0" == DB.get_first_value("select count(*) from #{table} where \
                              path = ? and last_seen > #{LIMIT}", path)
end

# Insert new entry or update it

INSERT = DB.prepare("insert or replace into files values (?,?,?,?,?,?,0)")
def insert(path,genre,artist,album,title)
  INSERT.execute(path,genre,artist,album,title,Time.now.tv_sec)
end

DIRUPDATE = DB.prepare("insert or replace into directories values (?,?,0)")
def update_dir(path)
  DIRUPDATE.execute(path,Time.now.tv_sec)
end


# Update failure counts and remove entries which failed too many times

def update_failures(prefix)
  DB.execute("update files set failures=failures+1 where \
               path like ? and last_seen < #{LIMIT}", prefix+"%")
  DB.execute("update directories set failures=failures+1 where \
               path like ? and last_seen < #{LIMIT}", prefix+"%")
end
def cleanup()
  DB.execute("delete from files where failures > #{$max_failures}")
  DB.execute("delete from directories where failures > #{$max_failures}")
end

# == Read audio tags ======================================================== #

def read(fname,vname)
  if updatable("files",vname) then
    begin
      file = TagLib::File.new(fname)
      puts fname
      insert(vname,
             file.genre.sub(/\s*$/,""),
             file.artist.sub(/\s*$/,""),
             file.album.sub(/\s*$/,""),
             file.title.sub(/\s*$/,""))
      file.close()
    rescue TagLib::BadTag => e
      # puts "#{fname} has bad tags"
      insert(vname,"?","?","?","?")
    rescue TagLib::BadFile
      # Ignore the many non-audio files
      # puts "Not audio: #{fname}"
    end
  else
    puts "#{fname} already up-to-date"
  end
end

# == Recursively parse a directory ========================================== #

def parse(root,prefix,rprefix,d)
  vname = prefix+d[rprefix..-1]
  if root or updatable("directories",vname) then
    begin
      puts d
      Dir.entries(d).each do |base|
        f = d+"/"+base
        if File.file? f then
          read(f,prefix+f[rprefix..-1])
        else
          if File.directory? f then
            if base[0..0] != "." then parse(false,prefix,rprefix,f) end
          else
            print "What to do with #{f} ?\n"
          end
        end
      end
      update_dir(vname)
    rescue SystemCallError => e
      puts "ERROR #{e}"
    end
  else
    puts "#{d} already up-to-date"
  end
end

# == Main =================================================================== #

$paths.each do |p|
  puts "Processing #{p[0]+p[2][p[1].length..-1]}..."
  parse(true,p[0],p[1].length,p[2])
  update_failures(p[0]+p[2][p[1].length..-1])
end
cleanup()

quotes = [
  "Sayonara !",
  "See you, space cowboy !",
  "I'll be back...",
  "Hasta la vista, baby.",
  "So long, and thanks for all the fish.",
  "Bye bye...",
  "To infinity and beyond !"
]
puts quotes[rand(quotes.length-1)]